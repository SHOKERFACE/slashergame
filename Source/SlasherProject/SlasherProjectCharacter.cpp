// Copyright Epic Games, Inc. All Rights Reserved.

#include "SlasherProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputActionValue.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "GenericPlatform/GenericPlatformInputDeviceMapper.h"

ASlasherProjectCharacter::ASlasherProjectCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = false; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bEnableCameraLag = false;
	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->TargetArmLength = 2000.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	FCoreDelegates::OnControllerConnectionChange.AddUObject(
		this, &ASlasherProjectCharacter::OnGameInputDeviceConnectionChange);

	//IPlatformInputDeviceMapper::GetOnInputDevicePairingChange();
}

void ASlasherProjectCharacter::Tick(float DeltaSeconds)
{
	APlayerController* myPC = Cast<APlayerController>(GetController());

	FHitResult TraceHitResult;
	myPC->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, TraceHitResult);
	if (TraceHitResult.bBlockingHit && !GamepadConnected && CanRotate)
	{
		FRotator LookRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(),
		                                                               FVector(TraceHitResult.Location.X,
		                                                                       TraceHitResult.Location.Y, 0.0f));
		FRotator TargetRotation = FMath::RInterpTo(GetActorRotation(),LookRotation,DeltaSeconds,RotationSpeed);
		SetActorRotation(FRotator(0.0f, TargetRotation.Yaw, 0.0f));
	}

	if (GamepadConnected)
	{
		GetCharacterMovement()->bOrientRotationToMovement = CanRotate;
	}

	Super::Tick(DeltaSeconds);
}

void ASlasherProjectCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void ASlasherProjectCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ASlasherProjectCharacter::Move);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Triggered, this, &ASlasherProjectCharacter::Jump);
		EnhancedInputComponent->BindAction(AttackAction, ETriggerEvent::Triggered, this,
		                                   &ASlasherProjectCharacter::Attack);
	}
}


void ASlasherProjectCharacter::Attack()
{
	CurrentCombo = AttackMontages.IsValidIndex(AttackCombo - 1) ? AttackCombo - 1 : 0;
	if (GetMesh()->GetAnimInstance()->Montage_IsPlaying(AttackMontages[CurrentCombo]))
	{
		AwaitingAttack = true;

		return;
	}
	if (!AttackMontages.IsValidIndex(AttackCombo)) { ResetCombo(); }

	PlayAnimMontage(AttackMontages[AttackCombo]);
	
	AttackCombo = AttackMontages.IsValidIndex(AttackCombo + 1) ? AttackCombo + 1 : 0;

	
	GetMesh()->GetAnimInstance()->OnMontageBlendingOut.AddDynamic(
		this, &ASlasherProjectCharacter::OnAttackMontageBlendingOut);

	GetWorld()->GetTimerManager().SetTimer(WaitingTimer, this, &ASlasherProjectCharacter::ResetCombo, AttackWaitingTime,
	                                       false, -1.0f);
}

void ASlasherProjectCharacter::OnAttackMontageBlendingOut(UAnimMontage* Montage, bool bInterrupted)
{
	GetMesh()->GetAnimInstance()->OnMontageBlendingOut.RemoveDynamic(
		this, &ASlasherProjectCharacter::OnAttackMontageBlendingOut);

	if (AwaitingAttack)
	{
		Attack();
	}
	AwaitingAttack = false;
	
}

void ASlasherProjectCharacter::ResetCombo()
{
	if (AwaitingAttack)
	{
		return;
	}
	AttackCombo = 0;
	GetWorld()->GetTimerManager().ClearTimer(WaitingTimer);
	UE_LOG(LogTemp, Verbose, TEXT("ComboReseted"));
}


void ASlasherProjectCharacter::Move(const FInputActionValue& Value)
{
	const FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		AddMovementInput(ForwardDirection, MovementVector.Y);
		AddMovementInput(RightDirection, MovementVector.X);
	}
}

void ASlasherProjectCharacter::OnGameInputDeviceConnectionChange(bool Connected, FPlatformUserId NewUserPlatformId,
                                                                 int32 ControllerIndex)
{
	if (!GetCharacterMovement() || !UGameplayStatics::GetPlayerController(GetWorld(), 0)) return;

	GamepadConnected = Connected;

	GetCharacterMovement()->bOrientRotationToMovement = Connected; // Rotate character to moving direction
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetShowMouseCursor(!Connected);
}

