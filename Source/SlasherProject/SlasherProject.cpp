// Copyright Epic Games, Inc. All Rights Reserved.

#include "SlasherProject.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SlasherProject, "SlasherProject" );

DEFINE_LOG_CATEGORY(LogSlasherProject)
 